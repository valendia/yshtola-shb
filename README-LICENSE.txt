IF YOU'RE READING THIS, YOU ARE PLANNING ON FORKING OFF THIS RELEASE
----------

Though I've released this under a Creative Commons Non-Derivative license, I won't stop you. You can go ahead and make a derivative of this theme, for as long as:

1.) You, under any circumstances, do not claim any art/character relating to Barbara and Genshin Impact as your own;
2.) You will attribute your work as a derivative of Sakura Index work
3.) Any element of code will not be used for commercial purposes. Exceptions are any element that cannot be identifiable or traced back to this release.

If you think you are okay with all of the above, then, goodluck!

If you got further questions, send it to crosse@sakuraindex.jp

OR follow me at twitter.com/crosse_

Cheers

Crosse